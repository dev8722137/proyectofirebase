// codigo JS

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB4VaVPgkyp-0qe-Jjdv_VKJoj3jezGXzM",
  authDomain: "paginasweb-fb81e.firebaseapp.com",
  projectId: "paginasweb-fb81e",
  storageBucket: "paginasweb-fb81e.appspot.com",
  messagingSenderId: "708798070552",
  appId: "1:708798070552:web:c8ba80996d4e209aaaa86d"
};

const app = initializeApp(firebaseConfig);
export {app}
export const auth = getAuth(app);
export const storage = getStorage(app)
